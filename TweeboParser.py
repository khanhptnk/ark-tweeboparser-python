#!/usr/bin/env python
# -*- coding: utf-8 -*-

import subprocess
import shlex
import os

TWEET_DEP_PARSER = os.environ["TWEET_DEP_PARSER"]
RUN_PARSER_CMD = os.path.join(TWEET_DEP_PARSER, "run.sh")

def _split_results(rows):
    for line in rows:
        line = line.strip("\n")  # remove '\n'
        if len(line) > 0:
            if line.count('\t') == 7:
                parts = line.split('\t')
                tokens = parts[1]
                tags = parts[3]
                head = int(parts[6])
                mwe = parts[-1]
                yield tokens, tags, head, mwe

def _call_runparser(tweets, run_parser_cmd=RUN_PARSER_CMD):
    # remove carriage returns as they are tweet separators for the stdin
    # interface
    tweets_cleaned = [tw.replace('\n', ' ') for tw in tweets]
    message = "\n".join(tweets_cleaned)

    # force UTF-8 encoding (from internal unicode type) to avoid .communicate encoding error as per:
    # http://stackoverflow.com/questions/3040101/python-encoding-for-pipe-communicate
    message = message.encode('utf-8')

    # build a list of args
    args = shlex.split(run_parser_cmd)
    po = subprocess.Popen(args, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    
    result = po.communicate(message)
    # expect a tuple of 2 items like:
    # ('hello\t!\t0.9858\nthere\tR\t0.4168\n\n',
    # 'Listening on stdin for input.  (-h for help)\nDetected text input format\nTokenized and tagged 1 tweets (2 tokens) in 7.5 seconds: 0.1 tweets/sec, 0.3 tokens/sec\n')
    parse_result = result[0].strip('\n\n')  # get first line, remove final double carriage return
    parse_result = parse_result.split('\n\n')  # split messages by double carriage returns
    parse_results = [pr.split('\n') for pr in parse_result]  # split parts of message by each carriage return
    return parse_results


def parse(tweets, run_parser_cmd=RUN_PARSER_CMD):
    parse_raw_results = _call_runparser(tweets, run_parser_cmd)
    parse_result = []
    for parse_raw_result in parse_raw_results:
        parse_result.append([x for x in _split_results(parse_raw_result)])
    return parse_result

